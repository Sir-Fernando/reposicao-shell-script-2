#!/bin/bash
IFS=$'\n'
mkdir arquivos

for linha in $(cat /etc/passwd); do
    user=$(echo $linha | cut -d ':' -f1)
    mkdir arquivos/$user
    echo $linha | cut -d ':' -f7 > arquivos/$user/interpretador.txt
done
