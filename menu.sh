#!/bin/bash

#Para a execução da opção de frases aleatórias, se faz necessário instalar o fortune.

function opcao() {

	echo "1) Criar um arquivo com frases aleatórias"
	echo "2) Exibir apenas os arquivos do diretório"
	echo "3) Exibir apenas os subdiretórios deste diretório"
	echo "4) Exibir apenas os executáveis deste diretório"
	echo "5) Renomear todos os arquivos deste diretório, colocando as letras em maiúsculas"
	echo "6) Renomear todos os arquivos deste diretório, colocando as letras em minúsculas"
	echo "7) Renomear todos os arquivos com extensão .txt para .docx"
	echo "8) Remover todos os arquivos com 3 linhas ou mais"
	echo "9) Para sair do script"

	read -p "Escolha uma opção: " opt
}

read -p "Informe um diretório: " dir
while [ "$opt" != "9" ]; do
	opcao
	case $opt in
	1) read -p "Digite a quantidade de frases que você deseja: " frases
		for ((i=1;i<=frases;i++)); do
			fortune >> frases.txt
		done
	;;
	2) ls -p $dir | grep -v / ;;
	3) ls -p $dir | grep / ;;
	4) find $dir -type f -executable ;;
	5) for f in $(ls -p $dir | grep -v /); do
		cd $dir
		mv $f $(echo $f | tr a-z A-Z)
	done ;;

	6) for f in $(ls -p $dir | grep -v /); do
		cd $dir
		mv $f $(echo $f | tr A-Z a-z)
	done ;;

	7) for f in $(ls -p $dir | grep -v /); do
		cd $dir
		mv $f $(echo $f | sed "s/.txt/.docx/")
	done ;;

	8) for f in $(ls -p $dir | grep -v /); do
		cd $dir
		if [ $(cat $f | wc -l) -ge 3 ]; then
			rm $f
		fi
	done ;;
	esac

done
